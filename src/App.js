import MainPage from "./Pages/MainPage";
import UserPage from "./Pages/UserPage";
import { UserProvider } from "./DataContext/DataContext";
import React from "react";
import Header from "./Components/Header";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from "./Pages/Login";
import Register from "./Pages/Register";
import Reset from "./Pages/Reset";
import {ToastContainer} from "react-toastify";
import Debug from "./Pages/Debug";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth } from "./Authentication/firebase";
import PrivateRoutes from "./Utils/PrivateRoutes";

function App() {
  const [user] = useAuthState(auth)

  return (
    <Router>
      <div className="App">
      <UserProvider
      >
        <Header />
        <Routes>
           <Route element={<PrivateRoutes user={ user } />}>
            <Route path="/debug" element={ <Debug/> }/>
            <Route path="/" element={ <MainPage /> }/>
            <Route path="/userpage" element={ <UserPage />} />
           </Route>
          <Route path="/login" element={ <Login/> }/>
          <Route path="/register" element={ <Register/> }/>
          <Route path="/reset" element={ <Reset/> }/>
          
        </Routes>
      </UserProvider>
      </div>
        <ToastContainer
            position="bottom-center"
            autoClose={1500}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="light"
        />
    </Router>

  );
}

export default App;
