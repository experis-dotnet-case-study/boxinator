import React, {useEffect, useState } from 'react'
import { DataGrid  } from '@mui/x-data-grid';
import { Button, FormControl, InputLabel, MenuItem, Select, Stack } from '@mui/material';
import axios from 'axios';
import { ShowNotificationError, ShowNotificationSuccess } from './Toasts';

function DebugTable() {
    const [selectionModel, setSelectionModel] = useState([]);
    const [rows, setRows] = useState([]);
    const [multiplier, setMultiplier] = useState('');
/*     const {shipmentId, setShipmentId} = useContext(DataContext);
 */

    const url = `${process.env.REACT_APP_API_URL}Settings/Countries`;

    const columns = [
        { field: 'id', headerName: 'ID', headerAlign: "center", align: 'center', width: 5 },
        { field: 'name', headerName: 'Country', headerAlign: "center", align: 'center', width: 100 },
        { field: 'multiplier', headerName: 'Multiplier', align: 'center', width: 75 }
    ];

    const handleCountryMultiplierChange = (event) => {
      setMultiplier(event.target.value);
    };

    // Get all countries
    const handleGetCountries = () => {
        axios
            .get(url)
            .then(res => {
                setRows(res.data);
            })
            .catch(err => {
                console.log(err)
                ShowNotificationError("Failed to load data. Please try again.");
            })
    };

    // Post the new country multiplier to backend
    const handleMultiplierChange = () => {
      const countryId = selectionModel[0];
      axios
          .put(`${url}/${countryId}` , {
              "multiplier": multiplier
          })
          .then(() => {
              ShowNotificationSuccess("New multiplier set")
              handleGetCountries();
          })
          .catch(err => {
              console.log(err)
              ShowNotificationError("Failed to set multiplier. Please try again.");
          })
  };
  
    useEffect(() => {
        handleGetCountries();
    });

  return (
    <div style={{ width: '311px', margin: "20px"}}>
      <h3>Countries</h3>
      <div style={{ height: 400, backgroundColor: 'rgb(250, 249, 246)'}}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
        onSelectionModelChange={(newSelectionModel) => {
            setSelectionModel(newSelectionModel);
          }}
        />
      <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-end'}}>
        <Stack flexDirection="row" justifyContent="flex-end" marginTop="10px">
            <FormControl sx={{ minWidth: "110px"}}>
            <InputLabel id="demo-simple-select-label">Multiplier</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={multiplier}
              label="Multiplier"
              onChange={handleCountryMultiplierChange}
            >
              <MenuItem value={0}>0</MenuItem>
              <MenuItem value={1}>1</MenuItem>
              <MenuItem value={1.5}>1.5</MenuItem>
              <MenuItem value={2}>2</MenuItem>
            </Select>
            </FormControl>
            <Button onClick={handleMultiplierChange}>Set multiplier</Button> 
        </Stack>
      </div>
    </div>
    </div>
      
  )
}

export default DebugTable