import React, { useContext } from "react";
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Box, Stack } from '@mui/system';
import { Link, useNavigate } from 'react-router-dom';
import {useAuthState} from "react-firebase-hooks/auth";
import {auth, logout} from "../Authentication/firebase";
import Logo from "../Assets/box-logo.png"
import { DataContext } from '../DataContext/DataContext';


export default function ButtonAppBar() {
    const [user] = useAuthState(auth);
    const navigate = useNavigate();
    const {accountType} = useContext(DataContext);

  return (
        <AppBar position="relative"	style={{ height: "60px" }}>
        <Toolbar sx={{justifyContent: "space-between"}}>
        <Stack
						style={{flex: 1, flexGrow: 1}}
            flexDirection="row"
            alignItems="center"
					>
						<img
							src={Logo}
              alt="Boxinator box logo"
							style={{height: "3em", cursor: "pointer"}}
							onClick={() => navigate("/")}
						/>
            <Typography 
              variant="h5"
              style={{cursor: "pointer"}}
              onClick={() => navigate("/")}>
            BOXINATOR
          </Typography>
					</Stack>
          <Box>
          
          {user && accountType === "0" && // Guest logged in
          <>
          <Button component={Link} to="/" color="inherit">Shipments</Button>
          </>
          }
          {user && accountType === "1" && // Normal user logged in
          <>
          <Button component={Link} to="/" color="inherit">Shipments</Button>
          <Button component={Link} to="/userpage" color="inherit">Profile</Button>
          </>
          }
          {user && accountType === "2" && // Admin logged in
          <>
          <Button component={Link} to="/debug" color="inherit">Admin</Button>
          <Button component={Link} to="/userpage" color="inherit">Profile</Button>
          </>
          }
          {user && // Any user logged in
          <>
          <Button component={Link} onClick={logout} color="inherit">Logout</Button>
          </>
          }
          </Box> 
        </Toolbar>
      </AppBar>
  );
}
