import React, { useState } from 'react';
import CancelIcon from "@mui/icons-material/Cancel";
import { TextField, Modal, Stack, Button, Typography, Box, IconButton } from '@mui/material';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
};

export default function GuestLoginModal() {
  const [open, setOpen] = useState(false);
  const [email, setEmail] = useState("");

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const closeModal = () => {
    handleClose();
    setEmail("");
    };

  const handleGuestLogin = (event) => {
    event.preventDefault();
   /*  axios
        .post(url, {
            email
        })
        .then((res) => {
            setEmail("");
            navigate("/");
        })
        .catch(() => alert("Something went wrong.")); */
};

  return (
    <div>
      <Button onClick={handleOpen}>Guest login</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
            <IconButton
                onClick={closeModal}
                tabIndex={-1}
                style={{
                    position: "absolute",
                    right: 0,
                    top: 0
                }}
            >
                <CancelIcon />
            </IconButton>

          <Typography sx={{ my: 2 }}>
            Email is required in order to receive receipts for the requested orders.
          </Typography>
          <form onSubmit={handleGuestLogin}>
            <Stack spacing={2}>
            <TextField
                    required
                    label="Email"
                    type="email"
                    value={email}
                    onChange={({ target }) => setEmail(target.value)}
                />
                <Button variant="contained" type="submit">
                Send
                </Button>
            </Stack>
            </form>
        </Box>
      </Modal>
    </div>
  );
}
