import {toast} from "react-toastify";

export const ShowNotificationSuccess = (props) => toast.success(props);
export const ShowNotificationError = (props) => toast.error(props);