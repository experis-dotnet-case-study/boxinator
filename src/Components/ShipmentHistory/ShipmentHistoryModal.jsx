import React, { useContext, useState } from 'react';
import CancelIcon from "@mui/icons-material/Cancel";
import { Modal, Button, Box, IconButton } from '@mui/material';
import ShipmentHistoryList from './ShipmentHistoryList';
import { Stack } from '@mui/system';
import { DataContext } from '../../DataContext/DataContext';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 300,
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
};

export default function ShipmentHistory() {
  const [open, setOpen] = useState(false);
  const {setShipmentId} = useContext(DataContext);

  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setShipmentId(null);
    setOpen(false);
  }

  const closeModal = () => {
    handleClose();
  };

  return (
    <div>
      <Button onClick={handleOpen} size="small">History</Button>
      <Modal
        open={open}
        onClose={handleClose}
      >
        <Box sx={style}>
            <IconButton
                onClick={closeModal}
                tabIndex={-1}
                style={{
                    position: "absolute",
                    right: 0,
                    top: 0
                }}
            >
                <CancelIcon />
            </IconButton>
            <Stack alignItems="center">
              <h2>
                Shipment history
              </h2>
              <ShipmentHistoryList />
            </Stack>
        </Box>  
      </Modal>
    </div>
  );
}
