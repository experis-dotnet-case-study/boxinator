import React, { useContext, useEffect, useState } from 'react'
import { DataGrid } from '@mui/x-data-grid';
import axios from 'axios';
import moment from 'moment';
import { DataContext } from '../../DataContext/DataContext';
import { ShowNotificationError } from '../Toasts';


function ShipmentHistoryList() {
  const [rows, setRows] = useState([]);
  const {shipmentId} = useContext(DataContext);

  const url = `${process.env.REACT_APP_API_URL}shipmenthistories?shipmentId=${shipmentId}`;

  const columns = [
    { field: 'shipmentStatusName', headerName: 'Status', headerAlign: "center", align: 'center', width: 90 },
    { field: 'timeStamp', headerName: 'Date & Time', align: 'center', headerAlign: "center", width: 135, valueFormatter: params =>
    moment.utc(params?.value).local().format("DD/MM/YYYY HH:mm") }
];

    useEffect(() => {
      if (shipmentId) {
        axios
      .get(url)
      .then(res => {
          setRows(res.data);
      })
      .catch(() => {
          ShowNotificationError("Failed to load data. Please try again.");
      })  
    }
    }, [shipmentId, url]);

  return (
    
      <div style={{ height: 400, width: "100%", maxWidth: '230px', backgroundColor: 'rgb(250, 249, 246)', margin: "20px" }}>
      <DataGrid
        rows={rows}
        columns={columns}
        getRowId={(row) => row.timeStamp}
        pageSize={5}
        rowsPerPageOptions={[5]}
        />
    </div>
  )
}

export default ShipmentHistoryList