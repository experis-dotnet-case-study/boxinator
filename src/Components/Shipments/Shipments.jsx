import React, { useContext, useEffect, useState } from 'react'
import axios from 'axios';
import { ShowNotificationError } from '../Toasts';
import { useAuthState } from 'react-firebase-hooks/auth';
import ShipmentsTable from './ShipmentsTable';
import { auth } from '../../Authentication/firebase';
import { DataContext } from '../../DataContext/DataContext';


function Shipments() {
  const [currentShipments, setCurrentShipments] = useState([]);
  const [completedShipments, setCompletedShipments] = useState([]);
  const [user] = useAuthState(auth);
  const {userShipments} = useContext(DataContext)

  useEffect(() => {
    if (user) {
      axios
        .get(`${process.env.REACT_APP_API_URL}shipments?firebaseAccountId=${user.uid}`)
        .then(res => {
          const complShipments = res.data.filter( x => x.shipmentStatusName === "Completed");
          const curShipments = res.data.filter( x =>
            x.shipmentStatusName === "Created" ||
            x.shipmentStatusName === "Received" ||
            x.shipmentStatusName === "InTransit");

          setCurrentShipments(curShipments);
          setCompletedShipments(complShipments);
        })
        .catch(() => {
            ShowNotificationError("Failed to load data. Please try again.");
        })
      }
    }, [user,userShipments]);

  return (
    <>
      <ShipmentsTable shipments={currentShipments} title="Current shipments" />
      <ShipmentsTable shipments={completedShipments} title="Completed shipments" />
    </>
  )
}

export default Shipments