import React, { useContext, useEffect, useState } from 'react'
import { GithubPicker} from 'react-color';
import { Form, Formik } from 'formik'
import '../../Styles/ShipmentModal.css'
import * as yup from "yup"
import { Button, TextField, Select, MenuItem, IconButton } from "@mui/material"
import axios from "axios"
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide'
import {DataContext} from "../../DataContext/DataContext";
import {auth} from "../../Authentication/firebase";
import CancelIcon from "@mui/icons-material/Cancel";


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
});

function ShipmentModal() {

  const validationSchema = yup.object().shape({
    receiver: yup.string("invalid name").matches(/^[a-zA-Z]/, "invalid name").required("name required"),
    tier: yup.string("choose a tier").matches(/^\d+$/).required("tier required"),
    destination: yup.string().matches(/^\d+$/).required("destination required")
  })

  //STATES
  const [shipmentData, setShipmentData] = useState("")
  const [visibility, setVisibility] = useState("hidden")
  const {setUserShipments} = useContext(DataContext)
  const [open, setOpen] = useState(false)
  const [values, setValues] = useState("")
  const [price, setPrice] = useState("")
  const [countries, setCountries] = useState([])

  useEffect(() => {
    if (values !== ""){
      getPrice()
    }
  }/* , [values] */)

  useEffect(() => {
    getCountries()
  }, [])

  //POST SHIPMENT TO DATABASE
  const postShipment = async(values) => {
    console.log("Creating shipment");
    var color = values.color.substring(1)
    var tier = parseInt(values.tier)
    var country = parseInt(values.destination)
    try{
      await axios.post(process.env.REACT_APP_API_URL + "Shipments", {
        "receiver" : values.receiver,
        "weightTier" : tier,
        "colour" : color,
        "firebaseAccountId" : auth.currentUser.uid,
        "countryId" : country
      })
      console.log("Shipment posted successfully");
    } catch(error){
      console.log("Shipment creation failed: " + error.message);
    }
  }

  //RETURN SHIPMENT'S PRICE FROM AN API
  const getPrice = async() => {
    var tier = parseInt(values.tier)
    var country = parseInt(values.destination)
    try{
      await axios.post(`${process.env.REACT_APP_API_URL}shipments/price`, {
        "countryId" : country,
        "weightTier" : tier
      })
          .then(res => {
            setPrice(res.data.price)
          })
    } catch(e){
      console.log("Price calculation failed " + e)
    }
  }

  const getCountries = async() => {
    try{
      await axios.get(`${process.env.REACT_APP_API_URL}settings/countries`)
          .then(res => {
            setCountries(res.data)
          })
    } catch(e){
      console.log("error retrieving countries " + e)
    }
  }

  //HANDLE OPENING AND CLOSING OF SHIPMENT MODAL
  const openShipmentModal = async() => {
    setVisibility("visible")
  }
  const closeShipmentModal = () => {
    setVisibility("hidden")
  }

  //SUBMIT SHIPMENT INTO DB AND REFRESH LIST OF SHIPMENTS
  const submitShipment = () => {
    postShipment(values)
        .then(axios.get(`${process.env.REACT_APP_API_URL}shipments/active?firebaseAccountId=${auth.currentUser.uid}`)
            .then(res => setShipmentData(res.data)))
        .then(() => setUserShipments(shipmentData))
        .then(setVisibility("hidden"))
  }

  //HANDLE CONFIRM MODAL ACTIONS
  const openConfirmModal = () => {
    setOpen(true)
  }
  const closeConfirmModal = () => {
    setOpen(false);
    setValues("")
  }
  const submitConfirmModal = () => {
    setOpen(false);
    submitShipment(values)
    setValues("")
  }


  const initialValues = {
    receiver: "",
    tier: "none",
    color: "#b80000",
    destination: "none"
  }
  return (
      <div>
        <Button variant="contained" onClick={openShipmentModal} style={{zIndex:0}}>Create Shipment</Button>

        <div className='shipmentModal'  style={{visibility:visibility}} >
        <IconButton
                onClick={closeShipmentModal}
                tabIndex={-1}
                style={{
                    position: "absolute",
                    right: 0,
                    top: 0
                }}
            >
                <CancelIcon />
            </IconButton>
          <Formik
              validateOnChange={false}
              initialValues={initialValues}
              validationSchema = {validationSchema}
              onSubmit = {(values, {resetForm}) => {
                setValues(values)
                openConfirmModal()
                resetForm()
              }}
          >
            {formik => (
                <Form className='shipmentForm'>

                  <h3>Place an Order</h3>

                  <TextField
                      error={Boolean(formik.errors.receiver)}
                      helperText={formik.errors.receiver}
                      name='receiver'
                      type={"text"}
                      placeholder="Receiver"
                      onChange={formik.handleChange}
                      value={formik.values.receiver}/>
                  <br/>
                  <Select
                      name='tier'
                      error={Boolean(formik.errors.tier)}
                      onChange={formik.handleChange}
                      value={formik.values.tier}>
                    <MenuItem value="none">Choose Tier</MenuItem>
                    <MenuItem value="0">Basic - 1kg</MenuItem>
                    <MenuItem value="1">Humble - 2kg</MenuItem>
                    <MenuItem value="2">Deluxe - 5kg</MenuItem>
                    <MenuItem value="3">Premium - 8kg</MenuItem>
                  </Select><br/>
                  <label>Box Color</label>
                  <div className='colorPicker'>
                    <GithubPicker
                        name='color'
                        triangle='top-left'
                        color={formik.values.color}
                        onChange = {updatedColor => formik.setFieldValue('color', updatedColor.hex)}
                    />
                  </div>

                  <Select
                      name='destination'
                      error={Boolean(formik.errors.destination)}
                      onChange={formik.handleChange}
                      value={formik.values.destination}>
                    <MenuItem value="none">Choose Destination</MenuItem>
                      {countries.map((country) => (
                          <MenuItem key={country.id} value={country.id}>{country.name}</MenuItem>
                      ))}
                  </Select><br/>

                  <Button variant="contained" type='submit'>Submit</Button>
                  <Button onClick={closeShipmentModal}>Cancel</Button>

                </Form>
            )}
          </Formik>

        </div>

        <Dialog
            open={open}
            TransitionComponent={Transition}
            keepMounted
            onClose={closeConfirmModal}
            aria-describedby="alert-dialog-slide-description">
          <DialogTitle>{"Confirm your order"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              Your shipment will cost <b>{price} eur</b>. Do you want to proceed?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button variant="contained" onClick={submitConfirmModal}>Confirm</Button>
            <Button onClick={closeConfirmModal}>Cancel</Button>
          </DialogActions>
        </Dialog>

      </div>
  )
}

export default ShipmentModal