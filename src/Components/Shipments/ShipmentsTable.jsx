import React, { useContext, useEffect, useState } from 'react'
import { DataGrid } from '@mui/x-data-grid';
import ShipmentHistory from '../ShipmentHistory/ShipmentHistoryModal';
import moment from 'moment';
import { Box } from '@mui/material';
import { DataContext } from '../../DataContext/DataContext';


function ShipmentsTable({shipments, title}) {
  const [rows, setRows] = useState([]);
  const {setShipmentId} = useContext(DataContext);

  useEffect(() => {
    setRows(shipments);
  }, [shipments]);

  const renderHistoryButton = () => <ShipmentHistory />;

  const columns = [
    { field: 'id', headerName: 'ID', headerAlign: "center", align: 'center', width: 5 },
    { field: 'shipmentStatusName', headerName: 'Status', headerAlign: "center", align: 'center', width: 80 },
    { field: 'receiver', headerName: 'Receiver', headerAlign: "center", align: 'center',  maxWidth: 140 },
    { field: 'countryName', headerName: 'Destination', align: 'center', width: 100 },
    { field: 'price', headerName: 'Price', align: 'center', width: 50 },
    { field: 'weightTierName', headerName: 'Weight tier', align: 'center', width: 85 },
    { field: 'colour', headerName: 'Colour', headerAlign: "center", align: 'center', width: 58 },
    { field: 'date', headerName: 'Created', headerAlign: "center", width: 132, valueFormatter: params =>
      moment.utc(params?.value).local().format("DD/MM/YYYY HH:mm") },
    { field: 'history', headerName: 'History', headerAlign: "center", width: 80, renderCell: renderHistoryButton }
  ];

  return (
    <div style={{ width: "800px", margin: "5px" }}>
      <h3>{title}</h3>
      <Box sx={{ height: 400, backgroundColor: 'rgb(250, 249, 246)',
        '& .darkRed': {
          backgroundColor: '#b80000', color: '#b80000',
        },
        '& .red': {
          backgroundColor: '#db3e00', color: '#db3e00',
        },
        '& .yellow': {
          backgroundColor: '#fccb00', color: '#fccb00',
        },
        '& .green': {
          backgroundColor: '#008b02', color: '#008b02',
        },
        '& .darkGreen': {
          backgroundColor: '#006b76', color: '#006b76',
        },
        '& .blue': {
          backgroundColor: '#1273de', color: '#1273de',
        },
        '& .darkBlue': {
          backgroundColor: '#004dcf', color: '#004dcf',
        },
        '& .purple': {
          backgroundColor: '#5300eb', color: '#5300eb',
        },
        '& .pink': {
          backgroundColor: '#eb9694', color: '#eb9694',
        },
        '& .lightPink': {
          backgroundColor: '#fad0c3', color: '#fad0c3',
        },
        '& .lightYellow': {
          backgroundColor: '#fef3bd', color: '#fef3bd',
        },
        '& .lightGreen': {
          backgroundColor: '#c1e1c5', color: '#c1e1c5',
        },
        '& .lighterGreen': {
          backgroundColor: '#bedadc', color: '#bedadc',
        },
        '& .lightBlue': {
          backgroundColor: '#c4def6', color: '#c4def6',
        },
        '& .lighterBlue': {
          backgroundColor: '#bed3f3', color: '#bed3f3',
        },
        '& .lightPurple': {
          backgroundColor: '#d3c3fb', color: '#d3c3fb',
        }}}>
        <DataGrid
          rows={rows}
          columns={columns}
          pageSize={5}
          rowsPerPageOptions={[5]}
          onSelectionModelChange={(newSelectionModel) => {
            setShipmentId(newSelectionModel);
          }}
          getCellClassName={(params) => {
            switch (params.value) {
              case "b80000":
                return "darkRed";
              case "db3e00":
                return "red";
              case "fccb00":
                return "yellow";
              case "008b02":
                return "green";
              case "006b76":
                return "darkGreen";
              case "004dcf":
                return "darkBlue";
              case "5300eb":
                return "purple";
              case "eb9694":
                return "pink";
              case "fad0c3":
                return "lightPink";
              case "fef3bd":
                return "lightYellow";
              case "c1e1c5":
                return "lightGreen";
              case "bedadc":
                return "lighterGreen";
              case "c4def6":
                return "lightBlue";
              case "bed3f3":
                return "lighterBlue";
              case "d3c3fb":
                return "lightPurple";
              case "1273de":
                return "blue";
              default:
                return "";
            }
        }}
          />
      </Box>
    </div>
  )
}

export default ShipmentsTable