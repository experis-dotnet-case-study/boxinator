import React, { useContext, useEffect, useState } from 'react'
import { DataGrid  } from '@mui/x-data-grid';
import { Button, FormControl, InputLabel, MenuItem, Select, Stack } from '@mui/material';
import axios from 'axios';
import { ShowNotificationError, ShowNotificationSuccess } from './Toasts';
import ShipmentHistory from './ShipmentHistory/ShipmentHistoryModal';
import moment from 'moment';
import { Box } from '@mui/system';
import { DataContext } from '../DataContext/DataContext';

function DebugTable() {
    const [rows, setRows] = useState([]);
    const [status, setStatus] = useState('');
    const {shipmentId, setShipmentId} = useContext(DataContext);

    const url = `${process.env.REACT_APP_API_URL}shipments`;

    const renderHistoryButton = () => <ShipmentHistory />;

    const columns = [
        { field: 'id', headerName: 'ID', headerAlign: "center", align: 'center', width: 5 },
        { field: 'shipmentStatusName', headerName: 'Status', headerAlign: "center", align: 'center', width: 80 },
        { field: 'firebaseAccountId', headerName: 'Sender', headerAlign: "center",  width: 100 },
        { field: 'receiver', headerName: 'Receiver', align: 'center', headerAlign: "center",  width: 140 },
        { field: 'countryName', headerName: 'Destination', width: 100 },
        { field: 'price', headerName: 'Price', align: 'center', width: 50 },
        { field: 'weightTierName', headerName: 'Weight tier', align: 'center', width: 85 },
        { field: 'colour', headerName: 'Colour', align: 'center', width: 65 },
        { field: 'date', headerName: 'Created', headerAlign: "center", width: 135, valueFormatter: params =>
        moment.utc(params?.value).local().format("DD/MM/YYYY HH:mm") },
        { field: 'history', headerName: 'History', headerAlign: "center", width: 85, renderCell: renderHistoryButton }
    ];
    
    const handleStatusMenuChange = (event) => {
      setStatus(event.target.value);
    };

    // Get all shipments
    const handleGetShipments = () => {
        axios
            .get(url + "/active")
            .then(res => {
                setRows(res.data);
            })
            .catch(err => {
                console.log(err)
                ShowNotificationError("Failed to load data. Please try again.");
            })
    };

    // Post the new shipment status to backend
    const handleStatusChange = () => {
        axios
            .put(`${url}/${shipmentId[0]}` , {
                "shipmentStatus": status
            })
            .then(() => {
                ShowNotificationSuccess("New status set")
                handleGetShipments();
            })
            .catch(err => {
                console.log(err)
                ShowNotificationError("Failed to set new status. Please try again.");
            })
    };

    useEffect(() => {
        handleGetShipments();
    });

  
  return (
    <div style={{ width: "900px", margin: "20px" }}>
      <h3>Current shipments</h3>
      <Box sx={{ height: 400, backgroundColor: 'rgb(250, 249, 246)',
        '& .darkRed': {
          backgroundColor: '#b80000', color: '#b80000',
        },
        '& .red': {
          backgroundColor: '#db3e00', color: '#db3e00',
        },
        '& .yellow': {
          backgroundColor: '#fccb00', color: '#fccb00',
        },
        '& .green': {
          backgroundColor: '#008b02', color: '#008b02',
        },
        '& .darkGreen': {
          backgroundColor: '#006b76', color: '#006b76',
        },
        '& .blue': {
          backgroundColor: '#1273de', color: '#1273de',
        },
        '& .darkBlue': {
          backgroundColor: '#004dcf', color: '#004dcf',
        },
        '& .purple': {
          backgroundColor: '#5300eb', color: '#5300eb',
        },
        '& .pink': {
          backgroundColor: '#eb9694', color: '#eb9694',
        },
        '& .lightPink': {
          backgroundColor: '#fad0c3', color: '#fad0c3',
        },
        '& .lightYellow': {
          backgroundColor: '#fef3bd', color: '#fef3bd',
        },
        '& .lightGreen': {
          backgroundColor: '#c1e1c5', color: '#c1e1c5',
        },
        '& .lighterGreen': {
          backgroundColor: '#bedadc', color: '#bedadc',
        },
        '& .lightBlue': {
          backgroundColor: '#c4def6', color: '#c4def6',
        },
        '& .lighterBlue': {
          backgroundColor: '#bed3f3', color: '#bed3f3',
        },
        '& .lightPurple': {
          backgroundColor: '#d3c3fb', color: '#d3c3fb',
        }}}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
        onSelectionModelChange={(newSelectionModel) => {
          setShipmentId(newSelectionModel);
        }}
        getCellClassName={(params) => {
            switch (params.value) {
              case "b80000":
                return "darkRed";
              case "db3e00":
                return "red";
              case "fccb00":
                return "yellow";
              case "008b02":
                return "green";
              case "006b76":
                return "darkGreen";
              case "004dcf":
                return "darkBlue";
              case "5300eb":
                return "purple";
              case "eb9694":
                return "pink";
              case "fad0c3":
                return "lightPink";
              case "fef3bd":
                return "lightYellow";
              case "c1e1c5":
                return "lightGreen";
              case "bedadc":
                return "lighterGreen";
              case "c4def6":
                return "lightBlue";
              case "bed3f3":
                return "lighterBlue";
              case "d3c3fb":
                return "lightPurple";
              case "1273de":
                return "blue";
              default:
                return "";
            }
        }}
        />
      <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-end'}}>
        <Stack flexDirection="row" justifyContent="flex-end" marginTop="10px">
            <FormControl sx={{ minWidth: "100px"}}>
            <InputLabel id="demo-simple-select-label">Status</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={status}
              label="Status"
              onChange={handleStatusMenuChange}
            >
              <MenuItem value={0}>Created</MenuItem>
              <MenuItem value={1}>Received</MenuItem>
              <MenuItem value={2}>In transit</MenuItem>
              <MenuItem value={3}>Completed</MenuItem>
              <MenuItem value={4}>Cancelled</MenuItem>
            </Select>
            </FormControl>
            <Button onClick={handleStatusChange}>Set status</Button>
        </Stack>
      </div>
    </Box>
    </div>
      
  )
}

export default DebugTable