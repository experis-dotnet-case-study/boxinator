import axios from "axios";
import {auth} from "../Authentication/firebase";
import {ShowNotificationError, ShowNotificationSuccess} from "../Components/Toasts";
import {DataContext} from '../DataContext/DataContext'

export const GetUserRequest = async() => {
    const firebaseId = auth.currentUser.uid;
    if(DataContext.User == null){
        await axios.get(`${process.env.REACT_APP_API_URL}Accounts/${firebaseId}`)
            .catch(async function(error){
                if(error.response && error.response.status === 404){
                    // Account not found (could happen if firebase authentication succeeded during account creation but database was not running so the account was not created there).
                    // The account is created here which is a bit hacky but fixes the edge case where the account has not been created during the initial flow.
                    await createUserRequest(auth.currentUser.uid, auth.currentUser.email);
                    ShowNotificationError("Failed to load data. Please try again.");
                } else{
                    ShowNotificationError("Something went wrong, please try again later.")
                }
                console.log(error.config);
            })
            .then((response) => {
                console.log("Received user data based on firebaseId. User email: " + response.data["email"]);
                DataContext.User = response.data;
            })
    } else{
        console.log(`User data has already been fetched.`);
    }
}

export const GetShipmentsRequest = async() => {
    try{
        if(DataContext.User == null){
            await GetUserRequest();
        }

        const url = `${process.env.REACT_APP_API_URL}Shipments?accountId=${DataContext.User["id"]}`;
        await axios.get(url)
            .then(resp => {
                DataContext.Shipments = resp.data;
                console.log(DataContext.Shipments);
                console.log(DataContext.Shipments[0]["date"]);
                console.log(DataContext.User);
            })
    } catch(error){
        console.log("Error fetching shipments: " + error.message);
    }
}

export const findUser = async(firebaseId, email) => {
    console.log("Checking if account exists for " + firebaseId);
    await axios.get(process.env.REACT_APP_API_URL + "Accounts/" + firebaseId)
        .catch(function(error) {
            if(error.response){
                if(error.response.status === 404){
                    console.log("Account not found, creating it now.");
                }
                else{
                    ShowNotificationError(`Something went wrong, please try again later ${error.response.status}.`);
                }
            }
            else{
                ShowNotificationError(`Something went wrong while fetching account, please try again later ${error}.`);
            }
        })
        .then(function(response){
           if(response.status === 200){
               if(auth.currentUser.displayName != null){
                   ShowNotificationSuccess(`Welcome back ${auth.currentUser.displayName}!`);
               }
               else{
                   ShowNotificationSuccess(`Welcome back!`);
               }
           }
           localStorage.setItem("accountType", response.data.accountType);
           localStorage.setItem("firebaseId", response.data.firebaseAccountId);
        });
}


export const createUserRequest = async(firebaseId, email) => {
    await axios.post(process.env.REACT_APP_API_URL + "Accounts", {
        "firebaseAccountId": firebaseId,
        "email": email,
        "accountType": 1
    })
    .then((response) => {
        console.log("Account created.");
        localStorage.setItem("accountType", response.data.accountType);
        localStorage.setItem("firebaseId", response.data.firebaseAccountId);
    })
}

export const CreateShipmentRequest = async(shipmentWeight, shipmentPrice, shipmentColor, shipmentDestination) => {
    if(DataContext.User == null){
        await GetUserRequest();
    }

    console.log("Creating shipment");
    try{
        await axios.post(process.env.REACT_APP_API_URL + "Shipments", {
            "accountId": DataContext.User["id"],
            "date": new Date().toISOString(),
            "weight": shipmentWeight,
            "price": shipmentPrice,
            "colour": shipmentColor.toString(),
            "countryId": shipmentDestination
        })

        console.log("Shipment succeeded");
    } catch(error){
        console.log("Shipment creation failed: " + error.message);
    }
}

export const SetAuthorizationHeader = () => {
    // Add a request interceptor
    axios.interceptors.request.use(
        config => {
            config.headers['Authorization'] = `Bearer ${localStorage.getItem('accessToken')}`;
            return config;
        },
        error => {
            return Promise.reject(error);
        }
    );
}