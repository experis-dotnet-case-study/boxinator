import { Stack } from '@mui/system'
import React from 'react'
import ShipmentModal from '../Components/Shipments/ShipmentModal'
import Shipments from '../Components/Shipments/Shipments'

function MainPage() {
  return (
    <div style={{marginTop: "30px",  height: "calc(100vh - 90px)"}}>
      <ShipmentModal/>
        <Stack 
          direction={{ xs: 'column', xl: 'row' }}
          justifyContent="center"
          alignItems="center">
          <Shipments/>
        </Stack>
    </div>
  )
}

export default MainPage