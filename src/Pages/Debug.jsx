import React from 'react'
import DebugTable from '../Components/DebugTable'
import DebugCountryMultiplierTable from '../Components/DebugCountryMultiplierTable'
import { Stack } from '@mui/system'

const Debug = () => {
  return (
    <div>
      <Stack
          direction={{ xs: 'column', xl: 'row' }}
          justifyContent="center"
          alignItems="center">
        <DebugTable/>
        <DebugCountryMultiplierTable/>
      </Stack>
    </div>
  )
}

export default Debug