import React, {useContext, useEffect, useState} from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { Link, useNavigate, useLocation } from "react-router-dom";
import {
    auth,
    registerWithEmailAndPassword,
    signInWithGoogle
} from "../Authentication/firebase";
import "../Styles/Register.css";
import { RecaptchaVerifier } from "firebase/auth";
import {DataContext} from "../DataContext/DataContext";
import {SetAuthorizationHeader} from "../Api/BackendManager";

function Register() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [name, setName] = useState("");
    const [phone, setPhone] = useState("");
    const [country, setCountry] = useState("");
    const [dob, setDob] = useState("");
    const [postalCode, setPostalCode] = useState("");
    const {accessToken, setAccessToken} = useContext(DataContext)
    const location = useLocation();

    const [user, loading] = useAuthState(auth);
    const navigate = useNavigate();
    const register = async() => {
        if(!name) alert("Please enter name");
        await registerWithEmailAndPassword(name, email, password, phone, country, dob, postalCode);
        await resolveAccessToken();
    };

    const resolveAccessToken = async() => {
        await auth.currentUser.getIdToken(true).then(function(idToken){
            localStorage.setItem("accessToken", idToken);
            SetAuthorizationHeader();
            setAccessToken(idToken);
        });
    }

    const [solvedRecaptcha, setSolvedRecaptcha] = useState(false);

    const googleSignIn = async() => {
        await signInWithGoogle();
        await resolveAccessToken();
    }

    function onSolvedRecaptcha() {
        console.log("Solved recaptcha!");
        setSolvedRecaptcha(true);
    }

    const from = location.state?.from || "/";
    useEffect(() => {
        if (loading) {
            return;
        }
        localStorage.setItem('accessToken', accessToken);
        if (user && accessToken != null && accessToken !== "null" && accessToken.length > 0) {
            navigate("/userpage")
        }
    }, [from, user, loading, navigate, accessToken]);
    useEffect(() => {
        if (loading) return;
        if (user) {
            navigate("/userpage", {replace: true});
        } else {
            const recaptchaVerifier = new RecaptchaVerifier(
                "recaptcha-container",

                // Optional reCAPTCHA parameters.
                {
                    "size": "normal",
                    "callback": function(response) {
                        // reCAPTCHA solved, you can proceed with
                        // phoneAuthProvider.verifyPhoneNumber(...).
                        onSolvedRecaptcha();
                    },
                    "expired-callback": function() {
                        setSolvedRecaptcha(false);
                        // Response expired. Ask user to solve reCAPTCHA again.
                        // ...
                    }
                }, auth
            );

            recaptchaVerifier.render()
                .then(function(widgetId) {
                    window.recaptchaWidgetId = widgetId;
                });
        }
    }, [user, loading, navigate]);
    return (
        <div className="register">
            {!solvedRecaptcha ?
                <div className="login">
                    <div className="login__container">
                        <p>Solve recaptcha to continue</p>
                        <div id={"recaptcha-container"}/>
                    </div>
                </div>
                :
                <div className="register__container">
                    <input
                        type="text"
                        className="register__textBox"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        placeholder="Full Name"
                    />
                    <input
                        type="text"
                        className="register__textBox"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="E-mail Address"
                    />
                    <input
                        type="text"
                        className="register__textBox"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                        placeholder="Phone number"
                    />
                    <input
                        type="text"
                        className="register__textBox"
                        value={country}
                        onChange={(e) => setCountry(e.target.value)}
                        placeholder="Country of Residence"
                    />
                    <input
                        type="text"
                        className="register__textBox"
                        value={postalCode}
                        onChange={(e) => setPostalCode(e.target.value)}
                        placeholder="Postal Code"
                    />
                    <input
                        type="date"
                        className="register__textBox"
                        value={dob}
                        onChange={(e) => setDob(e.target.value)}
                        placeholder="Date of birth"
                    />
                    <input
                        type="password"
                        className="register__textBox"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        placeholder="Password"
                    />
                    <button className="register__btn" onClick={register}>
                        Register
                    </button>
                    <button
                        className="register__btn register__google"
                        onClick={googleSignIn}
                    >
                        Register with Google
                    </button>
                    <div>
                        Already have an account? <Link to="/login">Login</Link> now.
                    </div>
                </div>
            }
        </div>

    );
}

export default Register;