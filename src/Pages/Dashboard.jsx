import React, { useEffect, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { useNavigate } from "react-router-dom";
import "../Styles/Dashboard.css";
import { auth, db, logout } from "../Authentication/firebase";
import { query, collection, getDocs, where } from "firebase/firestore";
// import firebase from "firebase/compat/app";
function Dashboard() {
    const [user, loading] = useAuthState(auth);
    const [name, setName] = useState("");
    const [phone, setPhone] = useState("");
    const [country, setCountry] = useState("");
    const [dob, setDob] = useState("");
    const [postalCode, setPostalCode] = useState("");
    const navigate = useNavigate();

    const fetchUserName = async () => {
        try {
            const q = query(collection(db, "users"), where("uid", "==", user?.uid));
            const doc = await getDocs(q);
            const data = doc.docs[0].data();
            setName(data.name);
            setPhone(data.phone);
            setCountry(data.country);
            setDob(data.dateOfBirth);
            setPostalCode(data.postalCode);
        } catch (err) {
            console.error(err);
            alert("An error occurred while fetching user data");
        }
    };
    useEffect(() => {
        if (loading) return;
        if (!user) return navigate("/");
        fetchUserName();
    }, [user, loading]);
    return (
        <div className="dashboard">
            <div className="dashboard__container">
                <div>Name: {name}</div>
                <div>Email: {user?.email}</div>
                <div>Contact number: {phone}</div>
                <div>Country of Residence: {country}</div>
                <div>Postal Code: {postalCode}</div>
                <div>Date of Birth: {dob}</div>
                <div>Email verified: {auth.currentUser != null && auth.currentUser.emailVerified ? "true" : "false"}</div>
                <button className="dashboard__btn" onClick={logout}>
                    Logout
                </button>
            </div>
        </div>
    );
}
export default Dashboard;