import React, {useEffect, useState} from 'react';
import {Form, Formik} from 'formik';
import * as yup from 'yup';
import {Button, TextField} from "@mui/material";
import moment from "moment";
import parse from "date-fns/parse";
import * as Yup from "yup";
import '../Styles/Profile.css'
import {auth, db, updateUser} from "../Authentication/firebase";
import {useAuthState} from "react-firebase-hooks/auth";
import {collection, getDocs, query, where} from "firebase/firestore";
import 'react-toastify/dist/ReactToastify.css';
import {sendEmailVerification, updatePassword} from "firebase/auth";
import {ShowNotificationError, ShowNotificationSuccess} from "../Components/Toasts";

function UserPage(){
    const [user, loading] = useAuthState(auth);
    const [userName, setUserName] = useState("");
    const [phone, setPhone] = useState("");
    const [country, setCountry] = useState("");
    const [dob, setDob] = useState("");
    const [postalCode, setPostalCode] = useState("");
    const [isUpdatingUser, setIsUpdatingUser] = useState(false);
    const [showPasswordPrompt, setShowPasswordPrompt] = useState(false);
    const [isUpdatingPassword, setIsUpdatingPassword] = useState(false);
    const [isSendingEmail, setIsSendingEmail] = useState(false);

    moment.locale('en');

    let initialProfileValues = {
        fullName: userName,
        dob: dob,
        country: country,
        postalCode: postalCode,
        contactNumber: phone,
        email: user?.email,
        password: 'foobarfoobar'
    }

    const passwordValidationSchema = yup.object({
        newPassword: yup
            .string()
            .min(8, 'Password should be of minimum 8 characters length')
            .required('Password is required'),
        confirmPassword: yup
            .string()
            .required('Password is required')
            .oneOf([yup.ref('newPassword'), null], 'Passwords must match')
    });

    const validationSchema = yup.object({
        fullName: yup
            .string()
            .required("Name is required"),
        dob: Yup.date()
            .transform(function(value, originalValue){
                if(this.isType(value)){
                    return value;
                }
                const result = parse(originalValue, "dd.MM.yyyy", new Date());
                return result;
            })
            .typeError("please enter a valid date")
            .required()
            .min("01.01.1900", "Date is too early")
            .max("31.12.2022", "Date should be earlier than 1.1.2023")
            .typeError("Please enter a valid date (format: 31.12.1980)")
            .required(),
        country: yup
            .string(),
        postalCode: yup
            .string()
            .matches(/^[0-9]+$/, "Postal code should only contain numbers"),
        contactNumber: yup
            .string()
            .matches(/^[+()0-9 ]+$/, "Contact number should only contain numbers"),
        email: yup
            .string()
            .email('Enter a valid email')
            .required('Email is required'),
        password: yup
            .string()
            .min(8, 'Password should be of minimum 8 characters length')
            .required('Password is required'),
    });

    const onSubmit = async(values) => {
        if(isUpdatingUser){
            return;
        }
        setIsUpdatingUser(true);
        await updateUser(values.fullName, values.contactNumber, values.country, values.dob, values.postalCode)
            .then(ShowNotificationSuccess("User information updated."))
            .catch(function(error){
                console.log("Update user failed: " + error.message);
            });
        setIsUpdatingUser(false);
    }

    const sendPasswordUpdate = async(values) => {
        console.log("Updating password 1");

        if(isUpdatingPassword === true){
            return;
        }
        console.log("Updating password 2");
        setIsUpdatingPassword(true);
        updatePassword(auth.currentUser, values.newPassword)
            .then(() => {
                ShowNotificationSuccess("Password updated.");
            }).catch((error) => {
            ShowNotificationError("Could not update password: " + error);
        })
            .then(() => setIsUpdatingPassword(false));
    }

    const sendVerification = () => {
        if(isSendingEmail){
            return;
        }
        setIsSendingEmail(true);
        sendEmailVerification(auth.currentUser)
            .then(function(){
                console.log("Verification email sent.");
                ShowNotificationSuccess("Email verification sent.")
            })
            .catch(function(error){
                if(error === "auth/too-many-requests"){
                    ShowNotificationError("Error sending email verification: too many requests. Please try again later.");
                } else{
                    console.log(error);
                    ShowNotificationError("Error sending email verification. Please try again later.");
                }
            });
        setIsSendingEmail(false);
    }

    const fetchUserName = async() => {
        try{
            const q = query(collection(db, "users"), where("uid", "==", user?.uid));
            const doc = await getDocs(q);
            const data = doc.docs[0].data();
            setUserName(data.name);
            setPhone(data.phone);
            setCountry(data.country);
            setDob(data.dateOfBirth);
            setPostalCode(data.postalCode);
        } catch(err){
            console.error(err);
            alert("An error occurred while fetching user data");
        }
    };

    useEffect(() => {
        if(loading) return;
        fetchUserName();
    }/* , [user, loading] */);
    if(showPasswordPrompt){
        return (
            <div>
                <br/>
                <Formik enableReinitialize={true} sx={{'& > :not(style)': {m: 15}}}
                        initialValues={{newPassword: "", confirmPassword: ""}}
                        validationSchema={passwordValidationSchema}
                        onSubmit={sendPasswordUpdate}
                >
                    {formik => (
                        <Form className="Form">
                            <TextField
                                className="TextField"
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                id="newPassword"
                                name="newPassword"
                                label="New password"
                                type="password"
                                value={formik.values.newPassword}
                                onChange={formik.handleChange}
                                error={formik.touched.newPassword && Boolean(formik.errors.newPassword)}
                                helperText={formik.touched.newPassword && formik.errors.newPassword}
                            />
                            <TextField
                                className="TextField"
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                id="confirmPassword"
                                name="confirmPassword"
                                label="Confirm password"
                                type="password"
                                value={formik.values.confirmPassword}
                                onChange={formik.handleChange}
                                error={formik.touched.confirmPassword && Boolean(formik.errors.confirmPassword)}
                                helperText={formik.touched.confirmPassword && formik.errors.confirmPassword}
                            />
                            <Button color="primary" variant="contained" className="UpdateButton" fullWidth
                                    type="submit" disabled={isUpdatingPassword}>Update password
                            </Button>
                        </Form>
                    )}
                </Formik>
                <div>
                    <br/>
                    <Button color="primary" variant="contained" className="UpdateButton" fullWidth
                            type="submit" disabled={isUpdatingPassword} onClick={() => setShowPasswordPrompt(false)}>Cancel
                    </Button>
                </div>
            </div>
        )
    } else{
        return (
            <div>
                <h2>PROFILE</h2>
                <Formik enableReinitialize={true} sx={{'& > :not(style)': {m: 15}}}
                        initialValues={initialProfileValues}
                        validationSchema={validationSchema}
                        onSubmit={onSubmit}
                >
                    {formik => (
                        <Form className="Form">
                            <TextField
                                className="TextField"
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                id="fullName"
                                name="fullName"
                                label="Full Name"
                                value={formik.values.fullName}
                                onChange={formik.handleChange}
                                error={formik.touched.fullName && Boolean(formik.errors.fullName)}
                                helperText={formik.touched.fullName && formik.errors.fullName}
                            />
                            <TextField
                                className="TextField"
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                control='date'
                                type={"date"}
                                id="dob"
                                name="dob"
                                label="Date of Birth"
                                value={formik.values.dob}
                                onChange={formik.handleChange}
                                error={formik.touched.dob && Boolean(formik.errors.dob)}
                                helperText={formik.touched.dob && formik.errors.dob}
                            />
                            <TextField
                                className="TextField"
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                id="country"
                                name="country"
                                label="Country"
                                value={formik.values.country}
                                onChange={formik.handleChange}
                                error={formik.touched.country && Boolean(formik.errors.country)}
                                helperText={formik.touched.country && formik.errors.country}
                            />
                            <TextField
                                className="TextField"
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                id="postalCode"
                                name="postalCode"
                                label="Postal Code"
                                value={formik.values.postalCode}
                                onChange={formik.handleChange}
                                error={formik.touched.postalCode && Boolean(formik.errors.postalCode)}
                                helperText={formik.touched.postalCode && formik.errors.postalCode}
                            />
                            <TextField
                                className="TextField"
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                id="contactNumber"
                                name="contactNumber"
                                label="Contact Number"
                                value={formik.values.contactNumber}
                                onChange={formik.handleChange}
                                error={formik.touched.contactNumber && Boolean(formik.errors.contactNumber)}
                                helperText={formik.touched.contactNumber && formik.errors.contactNumber}
                            />
                            <TextField
                                disabled={true}
                                className="TextField"
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                id="email"
                                name="email"
                                label="Email"
                                value={formik.values.email}
                                onChange={formik.handleChange}
                                error={formik.touched.email && Boolean(formik.errors.email)}
                                helperText={formik.touched.email && formik.errors.email}
                            />
                            <TextField
                                disabled={true}
                                className="TextField"
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                id="password"
                                name="password"
                                label="Password"
                                type="password"
                                value={formik.values.password}
                                onChange={formik.handleChange}
                                error={formik.touched.password && Boolean(formik.errors.password)}
                                helperText={formik.touched.password && formik.errors.password}
                            />
                            <Button color="primary" variant="contained" className="UpdateButton" fullWidth
                                    type="submit" disabled={isUpdatingUser}>Update Profile
                            </Button>
                        </Form>
                    )}
                </Formik>
                <div>
                    <br/>
                    <Button color="primary" variant="contained" className="UpdateButton" fullWidth
                            disabled={isUpdatingPassword} onClick={() => setShowPasswordPrompt(true)}>Change password
                    </Button>
                </div>
                {(auth.currentUser && !auth.currentUser.emailVerified) &&
                    <div>
                        <br/>
                        <Button color="warning" variant="contained" className="UpdateButton" fullWidth
                                type="submit" disabled={isSendingEmail} onClick={sendVerification}>Resend email
                            verification
                        </Button>
                    </div>
                }
            </div>
        );
    }
}

export default UserPage