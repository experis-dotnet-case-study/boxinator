import React, {useContext, useEffect, useState} from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { auth, logInWithEmailAndPassword, signInWithGoogle } from "../Authentication/firebase";
import { useAuthState } from "react-firebase-hooks/auth";
import "../Styles/Login.css";
import {DataContext} from "../DataContext/DataContext";
import {SetAuthorizationHeader} from "../Api/BackendManager";

function Login() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [user, loading] = useAuthState(auth);
    const navigate = useNavigate();
    const location = useLocation();
    // const storedAccessToken = localStorage.getItem("accessToken");
    // const [accessToken, setToken] = useState(storedAccessToken ? storedAccessToken : null);
    const {accessToken, setAccessToken} = useContext(DataContext)

    const handleKeypress = (e) => {
        //it triggers by pressing the enter key
        if (e.key === 'Enter') {
            logInWithEmailAndPassword(email, password);
        }
    };

    const resolveAccessToken = async() => {
        await auth.currentUser.getIdToken(true).then(function(idToken){
            localStorage.setItem("accessToken", idToken);
            SetAuthorizationHeader();
            setAccessToken(idToken);
        });
    }

    const googleSignIn = async() => {
        await signInWithGoogle();
        await resolveAccessToken();
    }

    const emailSignIn = async(email, password) => {
        await logInWithEmailAndPassword(email, password)
        await resolveAccessToken();
    }

    const from = location.state?.from || "/";
    useEffect(() => {
        if (loading) {
            return;
        }
        localStorage.setItem('accessToken', accessToken);
        if (user && accessToken != null && accessToken !== "null" && accessToken.length > 0) {
            navigate("/userpage")
        }
    }, [from, user, loading, navigate, accessToken]);
    return (
        <div className="login">
                <div className="login__container">
                    <input
                        type="text"
                        className="login__textBox"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="E-mail Address"
                    />
                    <input
                        type="password"
                        className="login__textBox"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        placeholder="Password"
                        onKeyPress={handleKeypress}
                    />
                    <button
                        className="login__btn"
                        onClick={() => emailSignIn(email, password)}
                    >
                        Login
                    </button>
                    <button className="login__btn login__google" onClick={googleSignIn}>
                        Login with Google
                    </button>
                    {/* <GuestLoginModal/> */}
                    <div>
                        <Link to="/reset">Forgot Password</Link>
                    </div>
                    <div>
                        Don't have an account? <Link to="/register">Register</Link> now.
                    </div>
                </div>
            </div>
    );
}

export default Login;