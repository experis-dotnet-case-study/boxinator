import { createContext, useEffect, useState } from "react";

export const DataContext = createContext(null)

export const UserProvider = ({ children }) => {
    const [userShipments, setUserShipments ] = useState(null);
	const [firebaseId, setFirebaseId] = useState(null);
	const [accountType, setAccountType] = useState(null);
	const storedAccessToken = localStorage.getItem("accessToken");
	const [accessToken, setAccessToken] = useState(storedAccessToken !== null && storedAccessToken !== "null" ? storedAccessToken : null);
	const [shipmentId, setShipmentId] = useState(null);

	useEffect(() => {
		if(accessToken){
			setAccessToken(localStorage.getItem('accessToken'));
			setAccountType(localStorage.getItem('accountType'));
			setFirebaseId(localStorage.getItem('firebaseId'));
		}
	}, [accessToken, setAccessToken]);
	return (
		<DataContext.Provider
			value={{
                userShipments,
                setUserShipments,
				accountType,
				firebaseId,
				setFirebaseId,
				setAccountType,
				setAccessToken,
				accessToken,
				shipmentId, 
				setShipmentId
			}}
		>
			{children}
		</DataContext.Provider>
	);
};
