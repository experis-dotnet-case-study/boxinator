import {
    GoogleAuthProvider,
    getAuth,
    signInWithPopup,
    signInWithEmailAndPassword,
    createUserWithEmailAndPassword,
    sendPasswordResetEmail,
    signOut,
    sendEmailVerification
} from "firebase/auth"
import {
    getFirestore,
    query,
    getDocs,
    collection,
    where,
    doc,
    setDoc
} from "firebase/firestore";
import firebaseConfig from "./firebase-config";

// Import the functions you need from the SDKs you need
import {initializeApp} from "firebase/app";
import {createUserRequest, findUser} from "../Api/BackendManager";
import {ShowNotificationError, ShowNotificationSuccess} from "../Components/Toasts";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);
const googleProvider = new GoogleAuthProvider();

const signInWithGoogle = async() => {
    try{
        const res = await signInWithPopup(auth, googleProvider);
        const user = res.user;
        const q = query(collection(db, "users"), where("uid", "==", user.uid));
        const docs = await getDocs(q);
        if(docs.docs.length === 0){
            await setDoc(doc(db, "users", user.uid), {
                uid: user.uid,
                name: user.displayName,
                authProvider: "google",
                email: user.email,
                phone: user.phoneNumber
            })
            console.log("Google user added to firestore.");
        }
        try{
            await(findUser(auth.currentUser.uid));
            
        }
        catch(error)
        {
            console.log(error.message);
            await createUserRequest(auth.currentUser.uid, auth.currentUser.email);
            console.log("Google user added to database.");
            if(auth.currentUser.displayName != null){
                ShowNotificationSuccess(`Welcome ${auth.currentUser.displayName}!`);
            }
            else{
                ShowNotificationSuccess(`Welcome!`);
            }
        }
    } catch(err){
        console.error(err);
        if(err.message === "Firebase: Error (auth/invalid-email)."){
            ShowNotificationError("Invalid email or password.");
        }
        else{
            ShowNotificationError("Something went wrong. Please try again.");
        }
    }
};

const logInWithEmailAndPassword = async(email, password) => {
    try{
        await signInWithEmailAndPassword(auth, email, password)
            .then(function(userCredential){
                console.log("Signed in successfully!");
            })
            .catch(function(error){
                if(error.code === 'auth/multi-factor-auth-required'){
                    console.log("MFA required.");
                }
                else if(error.code === 'auth/wrong-password'){
                    ShowNotificationError("Password or email is incorrect.");
                }
                else if(error.code === 'auth/user-not-found'){
                    ShowNotificationError("Password or email is incorrect.");
                }
                else{
                    console.log("Error: " + error.message);
                    ShowNotificationError("Something went wrong. Please try again.");
                }
            });
        if(auth.currentUser){
            try{
                console.log("Finding user " + auth.currentUser.uid);
                await(findUser(auth.currentUser.uid, auth.currentUser.email));
            }
            catch(error)
            {
                await createUserRequest(auth.currentUser.uid, auth.currentUser.email);
                if(auth.currentUser.displayName != null){
                    ShowNotificationSuccess(`Welcome ${auth.currentUser.displayName}!`);
                }
                else{
                    ShowNotificationSuccess(`Welcome!`);
                }
                console.log("Google user added to database.");
            }
        }
    } catch(err){
        console.error(err);
        if(err.message === "Firebase: Error (auth/invalid-email)."){
            ShowNotificationError("Invalid email or password.");
        }
        else{
            ShowNotificationError("Something went wrong. Please try again.");
        }
    }
};

const registerWithEmailAndPassword = async(name, email, password, phone, country, dateOfBirth, postalCode) => {
    try{
        await createUserWithEmailAndPassword(auth, email, password)
            .then(function(user){
                if(auth.currentUser && auth.currentUser.emailVerified === false){
                    console.log("Email has not been verified, sending a verification now.");
                    sendEmailVerification(auth.currentUser)
                        .then(function(){
                            console.log("Verification email sent.");
                            ShowNotificationSuccess("Email verification has been sent.\n\nMake sure to check your spam folder if you haven't received the message.");
                        })
                        .catch(function(error){
                            console.log("Error while sending verification email. " + error.toString());
                        });
                }
            });
        const user = auth.currentUser;

        await setDoc(doc(db, "users", user.uid), {
            uid: user.uid,
            name,
            authProvider: "local",
            email,
            phone: phone,
            country: country,
            dateOfBirth: dateOfBirth,
            postalCode: postalCode,
        })
        console.log("Firestore user added");
        await createUserRequest(auth.currentUser.uid, auth.currentUser.email);
        console.log("Database user created");
        ShowNotificationSuccess(`Welcome!`);
    } catch(err){
        console.error(err);
        ShowNotificationError("Something went wrong. Please try again.");
    }
};

const updateUser = async(newName, newPhone, newCountry, newDob, newPostalCode) => {
    try{
        console.log("Updating user with new name " + newName);
        const userData = {
            name: newName,
            phone: newPhone,
            country: newCountry,
            dateOfBirth: newDob,
            postalCode: newPostalCode
        }

        const userRef = doc(db, "users", auth.currentUser.uid );
        setDoc(userRef, userData, { merge: true})
            .then(userRef => {
                console.log("User updated successfully.")
            })
            .catch(error => {
                console.log("User update error: " + error);
            })
    } catch(err){
        console.error(err);
        ShowNotificationError("Something went wrong. Please try again.");
    }
};

const sendPasswordReset = async(email) => {
    try{
        await sendPasswordResetEmail(auth, email);
        ShowNotificationSuccess("Password reset link sent!\n\nMake sure to check your spam folder if you haven't received the message.");
    } catch(err){
        console.error(err);
        ShowNotificationError("Something went wrong. Please try again.");
    }
};

const logout = () => {
    signOut(auth);
    localStorage.clear();
};

export {
    auth,
    db,
    signInWithGoogle,
    logInWithEmailAndPassword,
    registerWithEmailAndPassword,
    sendPasswordReset,
    logout,
    updateUser
};