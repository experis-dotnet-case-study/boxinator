import { useAuthState } from 'react-firebase-hooks/auth';
import { Outlet, Navigate, useLocation } from 'react-router-dom'
import { auth } from '../Authentication/firebase';

const PrivateRoutes = () => {
    const [user, loading] = useAuthState(auth);
    const location = useLocation();
    
    return loading ? <></> :
        user
        ? <Outlet/>
        : (
            <Navigate
            to="/login"
            state={{ from: location }}
            replace/>
        )
}

export default PrivateRoutes
